const asyncHandler = require('express-async-handler')

const Note = require('../models/noteModel')

const getNotes = asyncHandler(async (req, res) => {
	const notes = await Note.find({ user: req.user.id })
	res.status(200).json(notes)
})

const setNote = asyncHandler(async (req, res) => {
	if (!req.body.title || !req.body.text) {
		res.status(400)
		throw new Error('Please add title and text field')
	}
	const note = await Note.create({
		title: req.body.title,
		text: req.body.text,
		user: req.user.id,
	})
	res.status(200).json(note)
})

const updateNote = asyncHandler(async (req, res) => {
	const note = await Note.findById(req.params.id)
	if (!note) {
		res.status(400)
		throw new Error('Note not found')
	}

	if (!req.user) {
		res.status(401)
		throw new Error('User not found')
	}

	if (note.user.toString() !== req.user.id) {
		res.status(401)
		throw new Error('User not authorized')
	}
	const updatedNote = await Note.findByIdAndUpdate(req.params.id, req.body, {
		new: true,
	})
	res.status(200).json(updatedNote)
})

const deleteNote = asyncHandler(async (req, res) => {
	const note = await Note.findById(req.params.id)
	if (!note) {
		res.status(400)
		throw new Error('Note not found')
	}

	if (!req.user) {
		res.status(401)
		throw new Error('User not found')
	}

	if (note.user.toString() !== req.user.id) {
		res.status(401)
		throw new Error('User not authorized')
	}

	await note.remove()
	res.status(200).json({ _id: req.params.id }) //returning id for frontend
})

const deleteAllNotes = asyncHandler(async (req, res) => {
	await Note.deleteMany({ user: req.user.id })
	res.status(200).json(user)
})

module.exports = {
	getNotes,
	setNote,
	updateNote,
	deleteNote,
	deleteAllNotes,
}
