const express = require('express')
const router = express.Router()
const {
	getNotes,
	setNote,
	updateNote,
	deleteNote,
	deleteAllNotes,
} = require('../controllers/noteController')

const { protect } = require('../middleware/authMiddleware')

//Get & Post note
router
	.route('/')
	.get(protect, getNotes)
	.post(protect, setNote)
	.delete(protect, deleteAllNotes)
//Update & Delete note
router.route('/:id').put(protect, updateNote).delete(protect, deleteNote)
//Delete All notes

module.exports = router
