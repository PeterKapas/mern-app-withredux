This is a full-stack ( MERN) Notes tracker application in the making

-register/login functionalities
-JWT, bcrypt, axios, protected routes
-redux toolkit
-styled components

Deployed app:

https://peternotesmernredux.herokuapp.com/
