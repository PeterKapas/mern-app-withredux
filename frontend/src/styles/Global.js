import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
 
* {
	margin: 0;
	padding: 0;
	box-sizing: border-box;
  
 
}

body {
  background: ${({ theme }) => theme.body};
  color: ${({ theme }) => theme.text};
  font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
  transition: all 0.30s linear;
  
  
}

button {
  cursor: pointer;
  & svg {
    height: 1.5rem;
    width: 1.5rem;
    color:${({ theme }) => theme.svg};
    text-align:center
  }
}
button:hover {
  scale:0.97
}
svg {
  color:${({ theme }) => theme.svg};
  height: 1.5rem;
  width: 1.5rem;
  margin-right: 0.4rem;
  cursor: pointer;
}
a {
	text-decoration: none;
	color:${({ theme }) => theme.a};
  cursor: pointer
}
a:hover {
  color: ${({ theme }) => theme.hover};
  scale:0.95
}
a:active  {
  scale: 1.2
}
::placeholder {
  color:${({ theme }) => theme.placeholder};
  font-style:oblique;
  opacity:0.5;

}

input, textarea {
  font-family: inherit;
	font-size: inherit;

}

h1{
  font-size:3rem;
  
  
}
h2{
  font-size:1.7rem
}
h3{
  font-size:1.4rem;
}
h4{
  font-size: 0.7rem;
  font-style: italic;
  margin-bottom: 0.3rem
}


p {
	line-height: 1.5;
  color: ${({ theme }) => theme.text};
  font-size: 1.5rem
}

ul {
	list-style: none;
}



`
