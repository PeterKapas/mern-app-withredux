import styled from 'styled-components'

export const Flex = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
`
export const FlexMargin = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	margin-bottom: 1rem;
`

export const SectionHeading = styled.section`
	font-size: 2rem;
	margin-bottom: 2rem;
	padding: 0 1rem;

	& svg {
		height: 2.5rem;
		width: 2.5rem;
	}
`
export const MainHeader = styled.nav`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 1.2rem;
	border-bottom: ${({ theme }) => theme.border};
	margin-bottom: 2.5rem;
	& ul {
		display: flex;
		align-items: center;
		justify-content: space-around;
		& > li {
			margin-left: 20px;
			& a {
				display: flex;
				align-items: center;
				& svg {
					margin-right: 5px;
				}
			}
		}
		& > li:first-child {
			margin-left: 0;
		}
	}
`

export const MainFooter = styled.footer`
	height: 50px;
	position: absolute;
	bottom: 0;
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	border-top: ${({ theme }) => theme.border};

	& ul {
		display: flex;
		align-items: center;
		justify-content: space-evenly;
		& > li {
			margin-left: 20px;
			& a {
				display: flex;
				align-items: center;
				& svg {
					margin-right: 5px;
				}
			}
		}
	}
`

export const SectionForm = styled.section`
	width: 70%;
	margin: 0 auto;
	display: flex;
	align-items: center;
	justify-content: center;
`
export const FormGroup = styled.div`
	width: 20rem;
	margin-bottom: 0.2rem;
`
export const LabelInput = styled.label`
	display: inline-block;
	margin: 0.4rem auto 0.6rem;
	font-style: inherit;
`

export const Input = styled.input`
	width: 100%;
	padding: 0.4rem;
	border: ${({ theme }) => theme.border};
	border-radius: 0.4rem;
	margin-bottom: 0.7rem;
	background-color: ${({ theme }) => theme.inputBgColor};
`

export const TextArea = styled.textarea`
	width: 100%;
	padding: 0.4rem;
	border: ${({ theme }) => theme.border};
	border-radius: 0.4rem;
	margin-bottom: 0.7rem;
	background-color: ${({ theme }) => theme.inputBgColor};
	resize: none;
`
export const UpdatedInput = styled.input`
	width: 85%;
	margin: 0.4rem;
	display: block;
	padding: 0.4rem;
	border: ${({ theme }) => theme.border};
	border-radius: 0.4rem;
	background-color: ${({ theme }) => theme.inputEdit};
	overflow: scroll;
`
export const UpdatedTextArea = styled.textarea`
	width: 85%;
	margin: 0.4rem;
	display: block;
	padding: 0.4rem;
	border: ${({ theme }) => theme.border};
	border-radius: 0.4rem;
	background-color: ${({ theme }) => theme.inputEdit};
	overflow: scroll;
	resize: none;
`
export const ButtonBlock = styled.button`
	padding: 0.6rem 1.2rem;
	border: ${({ theme }) => theme.border};
	border-radius: 0.5rem;
	background: ${({ theme }) => theme.buttonBgColor};
	color: ${({ theme }) => theme.buttonTextColor};
	font-size: 1.2rem;
	font-family: inherit;
	display: flex;
	align-items: center;
	justify-content: center;
	width: 50%;
	margin: 1rem auto;
`

export const ButtonMain = styled.button`
	padding: 0.6rem 1.2rem;
	border-radius: 0.5rem;
	background: ${({ theme }) => theme.buttonBgColor};
	color: ${({ theme }) => theme.buttonTextColor};
	font-size: 1.2rem;
	font-family: inherit;
	display: flex;
	align-items: center;
	justify-content: center;
	& svg {
		color: ${({ theme }) => theme.svgAlt};
	}
`
export const ButtonDeleteAll = styled.button`
	margin: 1rem auto;
	padding: 0.6rem 1.2rem;
	border-radius: 0.5rem;
	background: ${({ theme }) => theme.buttonBgColor};
	color: ${({ theme }) => theme.buttonTextColor};
	font-size: 1.2rem;
	font-family: inherit;
	display: flex;
	align-items: center;
	justify-content: center;
`

//NoteCard
export const NoteContainer = styled.div`
	background-color: ${({ theme }) => theme.inputBgColor};
	margin: 0.5rem;
	padding: 1rem 0.5rem 2rem;
	position: relative;
	min-height: 8rem;
	border: ${({ theme }) => theme.border};
	border-radius: 0.5rem;
`
export const AboutContainer = styled.div`
	background-color: ${({ theme }) => theme.inputBgColor};
	margin: 0.5rem;
	padding: 1rem 0.5rem 2rem;
	position: relative;
	min-height: 8rem;
	border: ${({ theme }) => theme.border};
	border-radius: 0.5rem;
`
export const ButtonClose = styled.button`
	position: absolute;
	top: 0.5rem;
	right: 0.5rem;
	border: none;
	background: none;
	& svg {
		margin: 0;
	}
`

export const ButtonEdit = styled.button`
	position: absolute;
	bottom: 0.5rem;
	right: 0.5rem;
	border: none;
	background: none;
	& svg {
		margin-right: 0;
	}
`
export const ButtonUpdate = styled.button`
	position: absolute;
	top: 0.5rem;
	right: 0.5rem;
	border: none;
	background: none;
	& svg {
		margin-right: 0;
	}
`
//DashBoard
export const DashView = styled.section`
	margin-bottom: 50px;
	& > div {
		margin-bottom: 2rem;
	}
`
export const DashContent = styled.section`
	width: 75%;
	margin: 0 auto;
`
export const DashNotes = styled.section`
	display: grid;
	grid-template-columns: repeat(2, minmax(0, 1fr));
	gap: 0.5rem;
`
export const DivFlow = styled.div``

export const BreakWordH2 = styled.h2`
	word-wrap: break-word;
	font-style: oblique;
	opacity: 0.9;
`
export const BreakWordH3 = styled.h3`
	word-wrap: break-word;
`

export const DarkModeButton = styled.button`
	display: flex;
	justify-content: center;
	align-items: center;
	text-align: center;
	height: 3rem;
	width: 3rem;
	border: none;
	background: none;
`
