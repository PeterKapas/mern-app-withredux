export const lightTheme = {
	body: '#FFF',
	text: '#363537',
	svg: '#363537',
	svgAlt: '#FFF',
	a: '#363537',
	hover: 'red',
	border: '2px solid #363537',
	buttonBgColor: '#363537',
	buttonTextColor: '#FFF',
	inputBgColor: '#f3f2f5',
	inputEdit: '#fac9aa',
	placeholder: '#363537',
}

export const darkTheme = {
	body: '#363537',
	text: '#FFF',
	svg: '#FFF',
	svgAlt: '#363537',
	a: '#FFF',
	hover: 'green',
	border: '2px solid #FFF',
	buttonBgColor: '#FFF',
	buttonTextColor: '#363537',
	inputBgColor: '#85838a',
	inputEdit: '#b0602f',
	placeholder: '#FFF',
}
