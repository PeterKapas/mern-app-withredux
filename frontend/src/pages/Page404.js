import { DashContent, DashView } from '../styles/StyledComponents'

const Page404 = () => {
	return (
		<DashView>
			<DashContent>
				<h1>
					I can neither confirm, nor deny, but this looks very much like a 404
					page 😵
				</h1>
			</DashContent>
		</DashView>
	)
}
export default Page404
