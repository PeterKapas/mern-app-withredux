import { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { login, reset } from '../features/auth/authSlice'
import Spinner from '../components/Spinner'
import { FaSignInAlt } from 'react-icons/fa'
import { Flex } from '../styles/StyledComponents'

import {
	SectionHeading,
	SectionForm,
	FormGroup,
	ButtonBlock,
	Input,
	FlexMargin,
} from '../styles/StyledComponents'

const Login = () => {
	const [formData, setFormData] = useState({
		email: '',
		password: '',
	})
	const navigate = useNavigate()

	const dispatch = useDispatch()

	const { user, isLoading, isError, isSuccess, message } = useSelector(
		(state) => state.auth
	)

	useEffect(() => {
		if (isError) {
			toast.error(message)
		}
		if (isSuccess || user) {
			navigate('/')
		}
		dispatch(reset())
	}, [user, isError, isSuccess, message, navigate, dispatch])

	const { email, password } = formData
	const onChange = (e) => {
		setFormData((prevState) => ({
			...prevState,
			[e.target.name]: e.target.value,
		}))
	}
	const onSubmit = (e) => {
		e.preventDefault()
		const userData = {
			email,
			password,
		}
		dispatch(login(userData))
	}

	if (isLoading) {
		return <Spinner />
	}
	return (
		<>
			<SectionHeading>
				<FlexMargin>
					<Flex>
						<FaSignInAlt />
						<h1>Login now</h1>
					</Flex>
				</FlexMargin>

				<h2>Please log in to your account</h2>
			</SectionHeading>
			<SectionForm>
				<form onSubmit={onSubmit}>
					<FormGroup>
						<Input
							type='email'
							id='email'
							name='email'
							value={email}
							placeholder='Enter your email'
							onChange={onChange}
						/>
					</FormGroup>
					<FormGroup>
						<Input
							type='password'
							id='password'
							name='password'
							value={password}
							placeholder='Enter your password'
							onChange={onChange}
						/>
					</FormGroup>
					<FormGroup>
						<ButtonBlock type='submit'>Submit</ButtonBlock>
					</FormGroup>
				</form>
			</SectionForm>
		</>
	)
}
export default Login
