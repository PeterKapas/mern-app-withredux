import {
	DashContent,
	DashView,
	AboutContainer,
} from '../styles/StyledComponents'

const About = () => {
	return (
		<DashView>
			<DashContent>
				<AboutContainer>
					<h1>Contact</h1>
					<h2>peter.kapas.1993@gmail.com</h2>
				</AboutContainer>
			</DashContent>
		</DashView>
	)
}
export default About
