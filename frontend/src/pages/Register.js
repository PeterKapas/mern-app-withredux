import { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { FaUser } from 'react-icons/fa'
import { register, reset } from '../features/auth/authSlice'
import Spinner from '../components/Spinner'
import {
	SectionHeading,
	SectionForm,
	FormGroup,
	ButtonBlock,
	Input,
	Flex,
	FlexMargin,
} from '../styles/StyledComponents'

const Register = () => {
	const [formData, setFormData] = useState({
		username: '',
		email: '',
		password: '',
		password2: '',
	})

	const { username, email, password, password2 } = formData

	const navigate = useNavigate()

	const dispatch = useDispatch()

	const { user, isLoading, isError, isSuccess, message } = useSelector(
		(state) => state.auth
	)

	useEffect(() => {
		if (isError) {
			toast.error(message)
		}
		if (isSuccess || user) {
			navigate('/')
		}
		dispatch(reset())
	}, [user, isError, isSuccess, message, navigate, dispatch])

	const onChange = (e) => {
		setFormData((prevState) => ({
			...prevState,
			[e.target.name]: e.target.value,
		}))
	}
	const onSubmit = (e) => {
		e.preventDefault()
		if (password !== password2) {
			toast.error('Passwords do not match')
		} else {
			const userData = {
				username,
				email,
				password,
				password2,
			}
			dispatch(register(userData))
		}
	}

	if (isLoading) {
		return <Spinner />
	}
	return (
		<>
			<SectionHeading>
				<FlexMargin>
					<Flex>
						<FaUser />
						<h1>Register now</h1>
					</Flex>
				</FlexMargin>
				<h2>Create your account</h2>
			</SectionHeading>
			<SectionForm>
				<form onSubmit={onSubmit}>
					<FormGroup>
						<Input
							type='text'
							id='name'
							name='username'
							value={username}
							placeholder='Enter your username'
							onChange={onChange}
							maxLength={20}
							required
						/>
					</FormGroup>
					<FormGroup>
						<Input
							type='email'
							id='email'
							name='email'
							value={email}
							placeholder='Enter your email'
							onChange={onChange}
							maxLength={64}
							required
						/>
					</FormGroup>
					<FormGroup>
						<Input
							type='password'
							id='password'
							name='password'
							value={password}
							placeholder='Enter your password'
							onChange={onChange}
							maxLength={20}
							required
						/>
					</FormGroup>
					<FormGroup>
						<Input
							type='password'
							id='password2'
							name='password2'
							value={password2}
							placeholder='Confirm your password'
							onChange={onChange}
							maxLength={20}
							required
						/>
					</FormGroup>
					<FormGroup>
						<ButtonBlock type='submit'>Submit</ButtonBlock>
					</FormGroup>
				</form>
			</SectionForm>
		</>
	)
}
export default Register
