import { MainFooter } from '../styles/StyledComponents'
import { Link } from 'react-router-dom'
import { FaLinkedin, FaGitlab, FaRegCopyright } from 'react-icons/fa'

const Footer = () => {
	return (
		<MainFooter>
			<div>
				<ul>
					<li>
						<Link to='/about'>
							<FaRegCopyright /> <h3>2022</h3>
						</Link>
					</li>
					<li>
						<a
							target='_blank'
							href='https://www.linkedin.com/in/p%C3%A9ter-kap%C3%A1s-32689912b/'
						>
							<FaLinkedin />
							<h3>Linkedin</h3>
						</a>
					</li>
					<li>
						<a target='_blank' href='https://gitlab.com/PeterKapas'>
							<FaGitlab /> <h3>GitLab</h3>
						</a>
					</li>
				</ul>
			</div>
		</MainFooter>
	)
}
export default Footer
