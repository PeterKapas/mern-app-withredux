import {
	DashContent,
	DashView,
	AboutContainer,
} from '../styles/StyledComponents'
import { useSelector } from 'react-redux'

const About = () => {
	const { user } = useSelector((state) => state.auth)
	return (
		<DashView>
			<DashContent>
				<AboutContainer>
					<h1>About this site</h1>
					<p>
						I hope you are having a great time exploring this little application
					</p>
				</AboutContainer>
				<AboutContainer>
					<p>This app was created using:</p>
					<ul>
						<li>React.js</li>
						<li>Node.js</li>
						<li>Express</li>
						<li>MongoDB</li>
					</ul>
				</AboutContainer>
				<AboutContainer>
					<p>It also involves the usage of:</p>
					<ul>
						<li>Redux Toolkit</li>
						<li>Styled components</li>
						<li>React Hooks</li>
						<li>React Toastify</li>
						<li>React Router</li>
						<li>React Icons</li>
						<li></li>
					</ul>
				</AboutContainer>
				<AboutContainer>
					<p>
						Do not hesitate to contact me if you have any questions or feedback
					</p>
					<p>Thank you {user && user.username} for visiting the site 😉</p>
				</AboutContainer>
			</DashContent>
		</DashView>
	)
}
export default About
