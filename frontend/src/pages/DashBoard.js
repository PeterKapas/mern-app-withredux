import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import NoteCard from '../components/NoteCard'
import NoteForm from '../components/NoteForm'
import Spinner from '../components/Spinner'
import { getNotes, deleteAllNotes } from '../features/notes/noteSlice'
import { reset } from '../features/auth/authSlice'
import {
	DashView,
	DashContent,
	DashNotes,
	ButtonDeleteAll,
} from '../styles/StyledComponents'

const DashBoard = () => {
	const navigate = useNavigate()
	const dispatch = useDispatch()
	const { user } = useSelector((state) => state.auth)
	const { notes, isError, isLoading, message } = useSelector(
		(state) => state.notes
	)

	useEffect(() => {
		if (isError) {
			console.log(message)
		}
	}, [isError, message])

	useEffect(() => {
		if (!user) {
			navigate('/login')
		}
	}, [user, navigate])

	useEffect(() => {
		dispatch(getNotes())
		return () => {
			dispatch(reset())
		}
	}, [dispatch])

	const deleteAll = () => {
		dispatch(deleteAllNotes())
	}

	if (isLoading) {
		return <Spinner />
	}

	return (
		<>
			<DashView>
				<div>
					<h1>Hello {user && user.username} ! </h1>
					<h2> Notes DashBoard</h2>
				</div>

				<NoteForm />
				<DashContent>
					{notes.length > 0 ? (
						<>
							<DashNotes>
								{notes.map((note) => (
									<NoteCard key={note._id} note={note} />
								))}
							</DashNotes>
							<ButtonDeleteAll onClick={deleteAll}>Delete all </ButtonDeleteAll>
						</>
					) : (
						<h3> You do not have any notes </h3>
					)}
				</DashContent>
			</DashView>
		</>
	)
}

export default DashBoard
