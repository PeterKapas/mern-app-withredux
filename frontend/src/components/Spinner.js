import styled from 'styled-components'

const Spinner = () => {
	return (
		<StyledSpinner>
			<div></div>
		</StyledSpinner>
	)
}
export default Spinner

const StyledSpinner = styled.div`
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background-color: transparent rgba(0, 0, 0, 0.5);
	z-index: 10;
	display: flex;
	justify-content: center;
	align-items: center;
	& div {
		width: 10rem;
		height: 10rem;
		border: 1rem solid;
		border-color: #000 transparent #555 transparent;
		border-radius: 50%;
		animation: spin 1.5s linear infinite;
		@keyframes spin {
			0% {
				transform: rotate(0deg);
			}
			100% {
				transform: rotate(360deg);
			}
		}
	}
`
