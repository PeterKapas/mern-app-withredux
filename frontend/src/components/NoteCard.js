import { useDispatch } from 'react-redux'
import { useState } from 'react'
import { deleteNote, updateNote } from '../features/notes/noteSlice'
import {
	FaTrashAlt,
	FaEdit,
	FaRegTimesCircle,
	FaRegCheckSquare,
} from 'react-icons/fa'
import {
	NoteContainer,
	ButtonClose,
	ButtonEdit,
	UpdatedInput,
	UpdatedTextArea,
	ButtonUpdate,
	DivFlow,
	BreakWordH2,
	BreakWordH3,
} from '../styles/StyledComponents'

//working on solution
const NoteCard = ({ note }) => {
	const [isEditMode, setIsEditMode] = useState(false)
	const [updatedNoteFormData, setUpdatedNoteFormData] = useState({
		updatedTitle: note.title,
		updatedText: note.text,
	})
	const { updatedTitle, updatedText } = updatedNoteFormData
	const dispatch = useDispatch()

	const onChange = (e) => {
		setUpdatedNoteFormData((prevState) => ({
			...prevState,
			[e.target.name]: e.target.value,
		}))
	}

	const toogleClick = () => {
		setIsEditMode((prevState) => !prevState)
	}

	const onSubmit = (e) => {
		e.preventDefault()
		const id = note._id
		const noteData = { title: updatedTitle, text: updatedText }
		dispatch(
			updateNote({
				id,
				noteData,
			})
		)
		toogleClick()
	}

	const formatDate = (date) => date.slice(0, date.indexOf(':') + 3)

	return (
		<NoteContainer>
			{isEditMode ? (
				<div>
					<form onSubmit={onSubmit}>
						<div>
							<BreakWordH2>Editing note...</BreakWordH2>
							<UpdatedInput
								placeholder='Update title...'
								type='text'
								name='updatedTitle'
								id='updatedTitle'
								value={updatedTitle}
								onChange={onChange}
								maxLength='32'
							/>
							<UpdatedTextArea
								placeholder='Update text...'
								type='textarea'
								name='updatedText'
								id='updatedText'
								value={updatedText}
								onChange={onChange}
								maxLength='128'
								rows='3'
							/>
						</div>

						<ButtonUpdate type='submit'>
							<FaRegCheckSquare />
						</ButtonUpdate>
					</form>
					<ButtonEdit onClick={toogleClick}>
						<FaRegTimesCircle />
					</ButtonEdit>
				</div>
			) : (
				<DivFlow>
					<h4>
						{`Added: ${formatDate(
							new Date(note.createdAt).toLocaleString('hu-HU')
						)}`}
					</h4>
					<BreakWordH2>{note.title}</BreakWordH2>
					<BreakWordH3>{note.text}</BreakWordH3>
					<ButtonClose onClick={() => dispatch(deleteNote(note._id))}>
						<FaTrashAlt />
					</ButtonClose>
					<ButtonEdit onClick={toogleClick}>
						<FaEdit />
					</ButtonEdit>
				</DivFlow>
			)}
		</NoteContainer>
	)
}
export default NoteCard
