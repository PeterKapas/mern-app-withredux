import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { createNote } from '../features/notes/noteSlice'
import { Input, TextArea } from '../styles/StyledComponents'
import {
	SectionForm,
	FormGroup,
	ButtonBlock,
	LabelInput,
} from '../styles/StyledComponents'

const NoteForm = () => {
	const [noteFormData, setNoteFormData] = useState({
		title: '',
		text: '',
	})

	const { title, text } = noteFormData
	const dispatch = useDispatch()

	const onSubmit = (e) => {
		e.preventDefault()
		dispatch(createNote({ title, text }))
		setNoteFormData({ title: '', text: '' })
	}

	const onChange = (e) => {
		setNoteFormData((prevState) => ({
			...prevState,
			[e.target.name]: e.target.value,
		}))
	}

	return (
		<SectionForm>
			<form onSubmit={onSubmit}>
				<FormGroup>
					<LabelInput htmlFor='title'>Note Title</LabelInput>
					<Input
						type='text'
						name='title'
						id='title'
						value={title}
						onChange={onChange}
						maxLength='32'
						placeholder='Add your title here...'
						required
					/>
					<LabelInput htmlFor='text'>Note Text</LabelInput>
					<TextArea
						type='textarea'
						name='text'
						rows='4'
						id='text'
						value={text}
						onChange={onChange}
						maxLength='128'
						placeholder='Add your note here...'
						required
					/>
				</FormGroup>
				<FormGroup>
					<ButtonBlock type='submit'> Add note</ButtonBlock>
				</FormGroup>
			</form>
		</SectionForm>
	)
}
export default NoteForm
