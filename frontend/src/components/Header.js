import {
	FaSignInAlt,
	FaSignOutAlt,
	FaUserCircle,
	FaStickyNote,
	FaQuestionCircle,
	FaMoon,
	FaSun,
	FaRegIdCard,
} from 'react-icons/fa'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { logout, reset } from '../features/auth/authSlice'
import {
	ButtonMain,
	MainHeader,
	DarkModeButton,
} from '../styles/StyledComponents'

const Header = ({ toggleTheme, mode }) => {
	const dispatch = useDispatch()
	const navigate = useNavigate()
	const { user } = useSelector((state) => state.auth)
	const onLogout = () => {
		dispatch(logout())
		dispatch(reset())
		navigate('/')
	}
	return (
		<MainHeader>
			<div>
				<ul>
					<li>
						<Link to='/'>
							<FaStickyNote /> <h2>Peter's Notes</h2>
						</Link>
					</li>
					<li>
						<Link to='/about'>
							<FaQuestionCircle />
							<h2>About</h2>
						</Link>
					</li>
					<li>
						<Link to='/contact'>
							<FaRegIdCard />
							<h2>Contact</h2>
						</Link>
					</li>
				</ul>
			</div>
			<div>
				{mode === 'light' ? (
					<DarkModeButton onClick={toggleTheme}>
						<FaMoon />
					</DarkModeButton>
				) : (
					<DarkModeButton onClick={toggleTheme}>
						<FaSun />
					</DarkModeButton>
				)}
			</div>
			<div>
				<ul>
					{user ? (
						<li>
							<ButtonMain onClick={onLogout}>
								<FaSignOutAlt /> Logout
							</ButtonMain>
						</li>
					) : (
						<>
							<li>
								<Link to='/login'>
									<FaSignInAlt /> <h2>Login</h2>
								</Link>
							</li>
							<li>
								<Link to='/register'>
									<FaUserCircle /> <h2>Register</h2>
								</Link>
							</li>
						</>
					)}
				</ul>
			</div>
		</MainHeader>
	)
}

export default Header
