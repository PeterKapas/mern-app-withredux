import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Header from './components/Header'
import Footer from './pages/Footer'
import DashBoard from './pages/DashBoard'
import Register from './pages/Register'
import Login from './pages/Login'
import About from './pages/About'
import Contact from './pages/Contact'
import Page404 from './pages/Page404'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import styled from 'styled-components'
import { ThemeProvider } from 'styled-components'
import { useState } from 'react'
import { GlobalStyles } from './styles/Global'
import { lightTheme, darkTheme } from './styles/Theme'

const App = () => {
	const [theme, setTheme] = useState('light')
	const toggleTheme = () => {
		theme === 'light' ? setTheme('dark') : setTheme('light')
	}
	return (
		<>
			<ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
				<GlobalStyles />
				<Router>
					<StyledContainer>
						<Header toggleTheme={toggleTheme} mode={theme} />
						<Routes>
							<Route path='/' element={<DashBoard />} />
							<Route path='/login' element={<Login />} />
							<Route path='/register' element={<Register />} />
							<Route path='about' element={<About />} />
							<Route path='/contact' element={<Contact />} />
							<Route path='*' element={<Page404 />} />
						</Routes>
						<Footer />
					</StyledContainer>
				</Router>
				<ToastContainer />
			</ThemeProvider>
		</>
	)
}
export default App

const StyledContainer = styled.div`
	width: 100%;
	max-width: 960px;
	margin: 0 auto;
	text-align: center;
	position: relative;
	min-height: 100vh;
	min-width: 680px;
	padding-bottom: 60px;
`
